var Metalsmith    = require('metalsmith');
var jstransformer = require('metalsmith-jstransformer');
var postcss       = require('metalsmith-postcss');
var yaml          = require('js-yaml');
var fs            = require('fs');

var opdrachten = yaml.safeLoad(fs.readFileSync('./opdrachten.yml', 'utf8'));
[ "af", "beschikbaar", "bezig", "afgekeurd" ].forEach(t => opdrachten[t].sort());

Metalsmith(__dirname)
	.metadata({
		site: {
			title: 'GVL iScout opdrachten'
		},
		opdrachten: opdrachten,
	})
	.source('./source')
	.destination('./public')
	.clean(true)
	.ignore(__dirname + '/source/css/_*')
	.use(postcss({
		plugins: [
			'postcss-easy-import',
			'postcss-nested'
		]
	}))
	.use(jstransformer())
	.build(function(err) {
		if (err) throw err;
	});
